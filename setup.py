import setuptools

from kabisa import version as kabisa_version

try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except (IOError, ImportError):
    with open("README.md", "r") as fh:
        long_description = fh.read()

setuptools.setup(
    name='kabisa-api',
    version=kabisa_version,
    scripts=["manage.py"],
    packages=setuptools.find_packages(),
    url='',
    license='GPLv3',
    author='Patrick van der Leer',
    author_email='pat.vdleer@gmail.com',
    description='Kabisa Quotes API',
    long_description=long_description,
    # long_description_content_type="text/markdown",
    classifiers=[
        'Intended Audience :: Developers',
        'Operating System :: POSIX',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    python_requires='>=3.6',
)
