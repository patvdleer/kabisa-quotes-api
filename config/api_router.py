from copy import copy

from django.conf import settings
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from kabisa import version
from kabisa.utils.api.schema import SchemaGenerator

app_name = ""
urlpatterns = [
    path('', include('kabisa.quotes.api_router')),
    path('users/', include('kabisa.users.api_router')),
    path('auth/', include('dj_rest_auth.urls')),
    path('auth/registration/', include('dj_rest_auth.registration.urls')),
    path('auth/multifactor/', include('kabisa.users.api.multifactor.api_router')),
]

schema_view = get_schema_view(
    openapi.Info(
        title="Kabisa Quote API",
        default_version=version,
        description="Kabisa Quote API",
        # terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="support@kabisa.com"),
        license=openapi.License(name="BSD License"),
    ),
    patterns=copy(urlpatterns),
    public=False,
    permission_classes=(
        permissions.IsAdminUser,
    ),
    generator_class=SchemaGenerator
)

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=600), name='schema-json'),
        re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=600), name='schema-swagger-ui'),
        re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=600), name='schema-redoc'),
    ]
