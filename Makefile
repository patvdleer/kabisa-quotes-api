ifeq ($(UNAME),Darwin)
	SHELL := /opt/local/bin/bash
	OS_X  := true
else ifneq (,$(wildcard /etc/redhat-release))
	OS_RHEL := true
else
	OS_DEB  := true
	SHELL := /bin/bash
endif

IP := $(shell command -v pip 2> /dev/null)
DOCKER := $(shell command -v docker 2> /dev/null)
DOCKER_COMPOSE := $(shell command -v docker-compose 2> /dev/null)
PWD := $(shell pwd)
DOCKER_CONFIG := $(shell [ -f ~/.docker/config.json ] && echo '$$HOME/.docker/config.json' || ( [ -f ~/snap/docker/current/.docker/config.json ] && echo '$$HOME/snap/docker/current/.docker/config.json' ) )
DOCKER_REGISTRY := registry.gitlab.com
GIT_BRANCH = $(shell git symbolic-ref --short HEAD)
HEROKU_PROJECT := 'kabisa-quotes-api'
POSTGRES_DB := kabisa-quotes-api
POSTGRES_USER := kabisa-quotes-api
POSTGRES_PASS := q63VpxcJf69MMRzJzcaVfyMa89SPh72T
GROUP_NAME := patvdleer
APP_NAME := kabisa-quotes-api
.PHONY: help
all: help

reset: ## Reset database and caches
	python manage.py reset_db --noinput
	python manage.py clear_cache
	python manage.py migrate
	for i in `ls fixtures/*.json | sort -V`; do python manage.py loaddata $$i; done;
	python manage.py createcachetable
	python manage.py check
	python manage.py import_quotes

setup: ## Run init setup
	python manage.py migrate
	for i in `ls fixtures/*.json | sort -V`; do python manage.py loaddata $$i; done;
	python manage.py createcachetable
	python manage.py check
	python manage.py import_quotes

dumpdata: ## Dump data from db to file
	 python manage.py dumpdata \
	 	-e contenttypes \
	 	-e auth.Permission \
	 	-e sessions \
	 	--natural-foreign \
	 	--natural-primary \
	 	--indent 4 > fixtures/0001-default.json

datadump: dumpdata ## Dump data from db to file

test: ## Run tests
	pytest .

format: ## AutoPEP8 format
	autopep8 -r -i -aa kabisa

makemessage: ## Make message
	python manage.py makemessages \
	  --ignore venv \
	  --ignore docs \
	  --locale nl_NL \
	  --locale de_DE \
	  --locale es_ES \
	  --locale fr_FR \
	  --locale en_EN

compilemessages: ## Compile messages
	python manage.py compilemessages

setup_postgres: ## Docker create postgres in Docker
	docker run --name postgres \
	  -e POSTGRES_DB=${POSTGRES_DB} \
	  -e POSTGRES_USER=${POSTGRES_USER} \
	  -e POSTGRES_PASSWORD=${POSTGRES_PASS} \
	  -p 5432:5432 \
	  -d postgres:13

docker-build: ## Docker build
	docker build --pull -t ${DOCKER_REGISTRY}/${GROUP_NAME}/${APP_NAME}/${GIT_BRANCH}:latest .

docker-pull: ## Docker pull
	docker pull ${DOCKER_REGISTRY}/${GROUP_NAME}/${APP_NAME}/${GIT_BRANCH}:latest

docker-push: ## Docker push
	docker push ${DOCKER_REGISTRY}/${GROUP_NAME}/${APP_NAME}/${GIT_BRANCH}:latest


help:
	@## http://www.network-science.de/ascii/ ; Font: starwars; Adjustment: center; Width: 80;
	@echo '  __  ___      ___      .______    __       _______.     ___      '
	@echo ' |  |/  /     /   \     |   _  \  |  |     /       |    /   \     '
	@echo ' |     /     /  ^  \    |  |_)  | |  |    |   (----`   /  ^  \    '
	@echo ' |    <     /  /_\  \   |   _  <  |  |     \   \      /  /_\  \   '
	@echo ' |  .  \   /  _____  \  |  |_)  | |  | .----)   |    /  _____  \  '
	@echo ' |__|\__\ /__/     \__\ |______/  |__| |_______/    /__/     \__\ '
	@echo ''
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ''
