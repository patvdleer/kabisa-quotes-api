# Kabisa Quotes API

## Makefile

```
  __  ___      ___      .______    __       _______.     ___      
 |  |/  /     /   \     |   _  \  |  |     /       |    /   \     
 |     /     /  ^  \    |  |_)  | |  |    |   (----`   /  ^  \    
 |    <     /  /_\  \   |   _  <  |  |     \   \      /  /_\  \   
 |  .  \   /  _____  \  |  |_)  | |  | .----)   |    /  _____  \  
 |__|\__\ /__/     \__\ |______/  |__| |_______/    /__/     \__\ 

compilemessages                Compile messages
datadump                       Dump data from db to file
docker-build                   Docker build
docker-pull                    Docker pull
docker-push                    Docker push
dumpdata                       Dump data from db to file
format                         AutoPEP8 format
makemessage                    Make message
reset                          Reset database and caches
setup_postgres                 Docker create postgres in Docker
setup                          Run init setup
test                           Run tests
```

## Tech

Kabisa Quotes API uses a number of open source projects to work properly:

* [python]
* [Django]
* [django-rest-framework]

## Installation

Requirements:
* [Python]
* [pip]
 
### VirtualEnv

#### Install
```sh
sudo -H pip3 install virtualenv 
```

### Create
```sh
virtualenv -p python3.8 venv
```

OR
```sh
python3.8 -m venv venv
```

### (de)Activate
```sh
source venv/bin/activate

deactivate
```

For more docs take a look [here](https://docs.python.org/3/library/venv.html)

## Dependencies

Install the dependencies and start the server.

```sh
pip install -r requirements-dev.txt
```

For production environments only...

```sh
pip install -r requirements.txt
```

## Development

Want to contribute? Great!

Kabisa Quotes API uses python3.6+ and django for fast developing.
Make a change in your file and instantaneously see your updates!

Open your favorite Terminal and run these commands.

### Init
```sh
python manage.py migrate
python manage.py loaddata fixtures/0001-default.json
python manage.py createcachetable
python manage.py import_quotes
python manage.py check
```

OR

```shell
make setup
```

### Run
```sh
python manage.py runserver
```

### Reset
```sh
python manage.py reset_db --noinput
python manage.py clear_cache
python manage.py clean_pyc
```

OR

```shell
make reset
```

## Usage

The entire API supports all major formats unless specifically specified otherwise.

* Browser interface
* MultiPart
* JSON
* XML
* YAML
* OpenAPI
* SchemaJS

### API Index

#### Root
* api/ ^$ [name='api-root']
* api/ ^\.(?P<format>[a-z0-9]+)/?$ [name='api-root']

#### Authors
* api/ ^authors/$ [name='api-authors-list']
* api/ ^authors\.(?P<format>[a-z0-9]+)/?$ [name='api-authors-list']
* api/ ^authors/(?P<id>[^/.]+)/$ [name='api-authors-detail']
* api/ ^authors/(?P<id>[^/.]+)\.(?P<format>[a-z0-9]+)/?$ [name='api-authors-detail']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/$ [name='api-quotes-authors-list']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-authors-list']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/(?P<id>[^/.]+)/$ [name='api-quotes-authors-detail']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/(?P<id>[^/.]+)\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-authors-detail']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/(?P<id>[^/.]+)/vote/down/$ [name='api-quotes-authors-vote-down']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/(?P<id>[^/.]+)/vote/down\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-authors-vote-down']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/(?P<id>[^/.]+)/vote/$ [name='api-quotes-authors-vote-up']
* api/ ^authors/(?P<authors_id>[^/.]+)/quotes/(?P<id>[^/.]+)/vote\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-authors-vote-up']

#### Quotes
* api/ ^quotes/$ [name='api-quotes-list']
* api/ ^quotes\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-list']
* api/ ^quotes/(?P<id>[^/.]+)/$ [name='api-quotes-detail']
* api/ ^quotes/(?P<id>[^/.]+)\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-detail']
* api/ ^quotes/(?P<id>[^/.]+)/vote/down/$ [name='api-quotes-vote-down']
* api/ ^quotes/(?P<id>[^/.]+)/vote/down\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-vote-down']
* api/ ^quotes/(?P<id>[^/.]+)/vote/$ [name='api-quotes-vote-up']
* api/ ^quotes/(?P<id>[^/.]+)/vote\.(?P<format>[a-z0-9]+)/?$ [name='api-quotes-vote-up']

#### Users/auth
* api/ users/
* api/ auth/
* api/ auth/registration/
* api/ auth/multifactor/

#### Documentation
* api/ ^swagger(?P<format>\.json|\.yaml)$ [name='schema-json']
* api/ ^swagger/$ [name='schema-swagger-ui']
* api/ ^redoc/$ [name='schema-redoc']


### Defaul Login

```
admin
kabisa
```

### Admin

http://127.0.0.1:8000/admin/


### API 

* Regular frontend endpoints
  http://127.0.0.1:8000/api/

* admin endpoints
  http://127.0.0.1:8000/api/admin/

#### Swagger - Frontend

* http://127.0.0.1:8000/api/swagger.json
* http://127.0.0.1:8000/api/swagger.yaml

##### Swagger docs

* Swagger-UI 
  http://127.0.0.1:8000/api/swagger/

* ReDoc 
  http://127.0.0.1:8000/api/redoc/
  

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [python]: <https://www.python.org/>
   [django]: <https://www.djangoproject.com/>
   [pip]: <https://pip.pypa.io/en/stable/installing/>
   [django-rest-framework]: <https://www.django-rest-framework.org/>
