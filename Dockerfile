# pull official base image
FROM python:3.8-slim

# set work directory
WORKDIR /usr/src/app

ENV DEBIAN_FRONTEND noninteractive
# install dependencies
RUN pip install --upgrade pip setuptools
COPY ./requirements.txt /usr/src/app/requirements.txt
# check if we need git to install requirements
RUN if [ "$(grep -c "^-e git" requirements.txt)" -ge 1 ]; then apt update && apt install -y git && apt-get -y clean; fi;
RUN pip install --no-cache-dir -r requirements.txt

# copy project
COPY . /usr/src/app/
EXPOSE 8000
STOPSIGNAL SIGINT
COPY docker/entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh / # backwards compat

RUN echo 1 > /init-done

# set environment variables
ENV DJANGO_DEBUG 0
ENV DJANGO_SETTINGS_MODULE "config.settings.production"
ARG DJANGO_SECRET_KEY

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["daphne", "-b", "0.0.0.0", "-p", "8000" ,"config.asgi:application"]
