# pull official base image
FROM python:3.8-slim

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV DEBIAN_FRONTEND noninteractive
RUN apt update && apt install -y git && apt-get -y clean

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DJANGO_DEBUG 1

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
COPY ./requirements-test.txt /usr/src/app/requirements-dev.txt
RUN pip install --no-cache-dir -r requirements-dev.txt

# copy project
COPY . /usr/src/app/
EXPOSE 8000
STOPSIGNAL SIGINT
COPY docker/entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
