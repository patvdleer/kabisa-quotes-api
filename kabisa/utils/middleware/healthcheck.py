import logging

from django.http import HttpResponse, HttpResponseServerError

logger = logging.getLogger("HealthCheck")


class HealthCheckMiddleware(object):
    """
    Healthcheck Middleware to inject into routing for usage in Kubernetes
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.method == "GET":
            if request.path == "/readiness":
                return self.readiness(request)
            elif request.path == "/liveness":
                return self.liveness(request)
            elif request.path == "/healthcheck":
                return self.healthcheck(request)
        return self.get_response(request)

    @staticmethod
    def healthcheck(request):
        """
        Returns a simple 200 OK that the server is alive.
        """
        return HttpResponse("OK")

    @staticmethod
    def liveness(request):
        """
        Returns a simple 200 OK that the server is alive.
        """
        return HttpResponse("OK")

    @staticmethod
    def readiness(request):
        """
        Check to see if the server is up and running

        Connect to each database and do a generic standard SQL query
        that doesn't write any data and doesn't depend on any tables
        being present.
        """
        try:
            from django.db import connections
            for name in connections:
                cursor = connections[name].cursor()
                cursor.execute("SELECT 1;")
                row = cursor.fetchone()
                if row is None:
                    return HttpResponseServerError("db: invalid response")
        except Exception as e:
            logger.exception(e)
            return HttpResponseServerError("db: cannot connect to database.")

        # XXX: disabled, possibly adapt this to Redis caching
        # Call get_stats() to connect to each memcached instance and get it's stats.
        # This can effectively check if each is online.
        # try:
        #     from django.core.cache import caches
        #     from django.core.cache.backends.memcached import BaseMemcachedCache
        #     for cache in caches.all():
        #         if isinstance(cache, BaseMemcachedCache):
        #             stats = cache._cache.get_stats()  # pylint: disable=W0212
        #             if len(stats) != len(cache._servers):  # pylint: disable=W0212
        #                 return HttpResponseServerError("cache: cannot connect to cache.")
        # except Exception as e:
        #     logger.exception(e)
        #     return HttpResponseServerError("cache: cannot connect to cache.")

        return HttpResponse("OK")
