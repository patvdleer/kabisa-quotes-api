from typing import List

from django.db.models import Model, ForeignKey, ImageField


def safe_filterset_fields(model: Model):
    default_exclude = (
        ForeignKey,
        ImageField,
    )
    return [f.name for f in model._meta.get_fields() if not f.is_relation and not isinstance(f, default_exclude)]


def filterset_exclude(model: Model, exclude: List[str]):
    return [f for f in safe_filterset_fields(model) if f not in exclude]
