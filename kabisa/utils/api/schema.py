import os

from drf_yasg.generators import OpenAPISchemaGenerator


class SchemaGenerator(OpenAPISchemaGenerator):
    url_fix = 'api/'

    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.basePath = os.path.join(schema.basePath, self.url_fix)
        return schema


class AdminSchemaGenerator(SchemaGenerator):
    url_fix = 'api/admin/'
