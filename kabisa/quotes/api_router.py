from django.conf import settings
from rest_framework_nested import routers

from kabisa.quotes.api.views.author import AuthorViewSet
from kabisa.quotes.api.views.quote import QuoteViewSet, QuoteNestedViewSet

if settings.DEBUG:
    router = routers.DefaultRouter()
    NestedRouter = routers.NestedDefaultRouter
else:
    router = routers.SimpleRouter()
    NestedRouter = routers.NestedSimpleRouter

router.register("quotes", QuoteViewSet, basename='quotes')
router.register("authors", AuthorViewSet, basename='authors')
author_router = NestedRouter(router, r'authors', lookup='authors')
author_router.register(
    r'quotes',
    QuoteNestedViewSet,
    basename='quotes-authors'
)

app_name = "api-quotes"


def clean(urls):
    return [u for u in urls if u.name != "api-root"]


urlpatterns = router.urls + clean(author_router.urls)
