from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class QuotesConfig(AppConfig):
    name = "kabisa.quotes"
    verbose_name = _("Quotes")

    def ready(self):
        try:
            import kabisa.quotes.signals  # noqa F401
        except ImportError:
            pass
