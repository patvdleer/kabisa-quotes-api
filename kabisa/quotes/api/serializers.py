from django.db.models import Sum
from rest_framework.fields import SerializerMethodField
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from kabisa.quotes.models import Quote, Author


class NestedAuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class AuthorSerializer(NestedAuthorSerializer):
    quotes = StringRelatedField(many=True, required=False)


class QuoteSerializer(ModelSerializer):
    author = NestedAuthorSerializer()
    score = SerializerMethodField('get_score')
    # votes = StringRelatedField()

    @staticmethod
    def get_score(obj):
        # there should be a better way than this
        votes = obj.votes.through.objects.filter(quote=obj).aggregate(Sum('value'))
        return votes['value__sum'] or 0

    def create(self, validated_data):
        if validated_data.get('author'):
            author_data = validated_data.get('author')
            author_serializer = NestedAuthorSerializer(data=author_data)

            if author_serializer.is_valid():
                author = Author.objects.get(**author_data)
                validated_data['author'] = author
                validated_data['author_id'] = author.id

        return super().create(validated_data)

    class Meta:
        model = Quote
        fields = '__all__'
