from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet

from kabisa.quotes.api.serializers import AuthorSerializer
from kabisa.quotes.models import Author


class AuthorViewSet(ModelViewSet):
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = "id"
