from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.decorators import action, permission_classes as permission_classes_decor
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_nested.viewsets import NestedViewSetMixin

User = get_user_model()

from kabisa.quotes.api.serializers import QuoteSerializer
from kabisa.quotes.models import Quote, UserQuoteVote


class QuoteViewSet(ModelViewSet):
    serializer_class = QuoteSerializer
    queryset = Quote.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = "id"

    def get_queryset(self, *args, **kwargs):
        return self.queryset

        # ToDO

        return self.queryset.select_related("votes")


    @action(detail=True, methods=["GET"], url_path="vote/down")
    @permission_classes_decor([IsAuthenticated])
    def vote_down(self, request, *args, **kwargs) -> Response:
        instance = self.get_object()
        self._vote(request.user, instance, -1)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=["GET"], url_path="vote/up")
    @permission_classes_decor([IsAuthenticated])
    def vote_up(self, request, *args, **kwargs) -> Response:
        instance = self.get_object()
        self._vote(request.user, instance, +1)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=["GET"], url_path="unvote")
    @action(detail=True, methods=["GET"], url_path="vote/reset")
    @permission_classes_decor([IsAuthenticated])
    def vote_reset(self, request, *args, **kwargs) -> Response:
        instance = self.get_object()
        self._vote(request.user, instance, 0)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=["POST"], url_path="vote")
    @permission_classes_decor([IsAuthenticated])
    def vote(self, request, *args, **kwargs) -> Response:
        instance = self.get_object()
        self._vote(request.user, instance, 1)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @staticmethod
    def _vote(user: User, quote: Quote, value):
        vote, _ = UserQuoteVote.objects.get_or_create(
            user=user,
            quote=quote,
            defaults={
                "value": value
            }
        )
        if vote.value != value:
            vote.value = value
            vote.save()
        return vote


class QuoteNestedViewSet(NestedViewSetMixin, QuoteViewSet):
    def get_queryset(self, *args, **kwargs):
        _filter = {}
        if "authors_id" in self.kwargs:
            _filter['author'] = self.kwargs['authors_id']
        return self.queryset.filter(**_filter)
