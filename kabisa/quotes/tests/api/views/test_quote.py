from unittest import TestCase

import pytest
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from kabisa.quotes.models import Author
from kabisa.users.tests.factories import UserFactory

User = get_user_model()

pytestmark = pytest.mark.django_db


class QuoteApiTestCase(TestCase):
    api_client: APIClient
    model_factory: UserFactory = UserFactory
    model: User = User

    def setUp(self) -> None:
        self.api_client = APIClient()

    def test_get_list(self) -> None:
        response = self.api_client.get(
            reverse('api-quotes:quotes-list'),
            format='json'
        )
        self.assertEqual(response.status_code, 200)

        user = self.model_factory()
        self.api_client.force_authenticate(user=user)
        response = self.api_client.get(
            reverse('api-quotes:quotes-list'),
            format='json'
        )
        self.assertEqual(response.status_code, 200)

    def test_create(self) -> None:
        author, _ = Author.objects.get_or_create(name="Patrick van der leer")
        data = {
            "text": "bla bla",
            "author": {
                "name": "Patrick van der leer"
            }
        }

        response = self.api_client.post(
            reverse('api-quotes:quotes-list'),
            format='json',
            data=data
        )
        self.assertEqual(response.status_code, 403)

        user = self.model_factory()
        self.api_client.force_authenticate(user=user)
        response = self.api_client.post(
            reverse('api-quotes:quotes-list'),
            format='json',
            data=data
        )
        self.assertEqual(response.status_code, 201, str(response.data))

    def test_voting(self) -> None:
        author, _ = Author.objects.get_or_create(name="Patrick van der leer")
        data = {
            "text": "bla bla",
            "author": {
                "name": "Patrick van der leer"
            }
        }

        user = self.model_factory()
        self.api_client.force_authenticate(user=user)
        response = self.api_client.post(
            reverse('api-quotes:quotes-list'),
            format='json',
            data=data
        )
        self.assertEqual(response.status_code, 201, str(response.data))
        data = response.data

        response = self.api_client.get(
            reverse('api-quotes:quotes-detail', kwargs={"id": data['id']}),
            format='json',
        )
        self.assertEqual(response.status_code, 200, str(response.data))

        response = self.api_client.get(
            reverse('api-quotes:quotes-vote-up', kwargs={"id": data['id']}),
            format='json',
        )
        self.assertEqual(response.status_code, 204, str(response.data))

        response = self.api_client.get(
            reverse('api-quotes:quotes-vote-down', kwargs={"id": data['id']}),
            format='json',
        )
        self.assertEqual(response.status_code, 204, str(response.data))

        response = self.api_client.get(
            reverse('api-quotes:quotes-vote-reset', kwargs={"id": data['id']}),
            format='json',
        )
        self.assertEqual(response.status_code, 204, str(response.data))

        response = self.api_client.get(
            reverse('api-quotes:quotes-detail', kwargs={"id": data['id']}),
            format='json',
        )
        self.assertEqual(response.status_code, 200, str(response.data))
