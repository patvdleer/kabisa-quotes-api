from django.contrib import admin

from kabisa.quotes.models import Quote, Author


class QuoteAdminInline(admin.StackedInline):
    model = Quote
    extra = 0


@admin.register(Quote)
class QuoteAdmin(admin.ModelAdmin):
    pass


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    inlines = (
        QuoteAdminInline,
    )
