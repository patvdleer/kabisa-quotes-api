import requests
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import gettext_lazy as _

from kabisa.quotes.api.serializers import AuthorSerializer, QuoteSerializer
from kabisa.quotes.models import Quote, Author


class Command(BaseCommand):
    help = 'Import the quotes from the external source'

    @staticmethod
    def get_data():
        response = requests.get("http://quotes.stormconsultancy.co.uk/quotes.json")
        assert response.status_code == 200, _("Unable to get quotes from source")
        return response.json()

    def import_data(self, data: dict):
        for entry in data:
            try:
                author, _ = Author.objects.get_or_create(
                    name=entry['author']
                )
            except KeyError:
                self.stderr.write(self.style.ERROR('Entry incomplete, skipping'))
                continue
            except Exception as e:
                self.stderr.write(self.style.ERROR(str(e)))
                continue

            try:
                quote = Quote.objects.get(
                    text=entry['quote']
                )
                if quote.author is not author:
                    self.stderr.write(
                        self.style.ERROR(
                            'The following quote seems to have switched authors; "%s" from author "%s" to "%s"' % (
                                entry['quote'],
                                author,
                                quote.author,
                            )
                        )
                    )
                    continue
            except Quote.DoesNotExist:
                Quote.objects.create(
                    text=entry['quote'],
                    author=author
                )
            except Quote.MultipleObjectsReturned:
                self.stderr.write(
                    self.style.ERROR('Multiple entries found for the following quote: "%s"' % entry['quote'])
                )
                continue
            except Exception as e:
                self.stderr.write(self.style.ERROR(str(e)))
                continue

    def handle(self, *args, **options):
        try:
            self.import_data(self.get_data())
        except Exception as e:
            raise CommandError(str(e))

        self.stdout.write(self.style.SUCCESS('Successfully imported quotes'))
