from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

User = get_user_model()


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class Author(models.Model):
    name = models.CharField(
        _('Name'),
        max_length=254
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Quote(models.Model):
    text = models.TextField(
        _('Text'),
    )

    author = models.ForeignKey(Author, related_name='quotes', on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    votes = models.ManyToManyField(User, through='UserQuoteVote')

    def __str__(self):
        return self.text


class UserQuoteVote(models.Model):
    quote = models.ForeignKey(Quote, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.SET(get_sentinel_user))
    value = models.SmallIntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.value == 0:
            return f"{self.user} unvoted"

        return f"{self.user} {'up' if self.value > 0  else 'down'}voted "
