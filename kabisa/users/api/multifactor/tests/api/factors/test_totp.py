import pyotp
from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

User = get_user_model()


class TotpApiTestCase(TestCase):
    api_client: APIClient

    def setUp(self) -> None:
        self.api_client = APIClient()

    def test_create(self) -> None:
        user = User.objects.create_user(
            username='testuser',
            email='testuser@example.com',
            password='12345'
        )
        response = self.api_client.get(
            reverse('multifactor-drf-totp-list'),
            format='json'
        )
        self.assertEqual(response.status_code, 403)

        self.api_client.force_authenticate(user=user)
        response = self.api_client.get(
            reverse('multifactor-drf-totp-list'),
            format='json'
        )
        self.assertEqual(response.status_code, 200)

        response = self.api_client.get(
            reverse('multifactor-drf-totp-qr'),
            format='json'
        )
        self.assertEqual(response.status_code, 200)

        self.assertIsInstance(response.data, dict)

        self.assertTrue("qr" in response.data.keys())
        self.assertTrue("secret_key" in response.data.keys())

        secret_key = response.data["secret_key"]

        response = self.api_client.post(
            reverse('multifactor-drf-totp-list'),
            format='json',
            data={
                "answer": "123456"
            }
        )
        self.assertEqual(response.status_code, 500)

        response = self.api_client.post(
            reverse('multifactor-drf-totp-list'),
            format='json',
            data={
                "answer": "123456",
                "secret_key": secret_key,
            }
        )
        self.assertEqual(response.status_code, 500)

        totp = pyotp.TOTP(secret_key)
        answer = totp.now()

        response = self.api_client.post(
            reverse('multifactor-drf-totp-list'),
            format='json',
            data={
                "answer": answer,
                "secret_key": secret_key,
            }
        )
        self.assertEqual(response.status_code, 201)

        self.api_client.force_authenticate(user=user)
        response = self.api_client.get(
            reverse('multifactor-drf-totp-list'),
            format='json'
        )
        self.assertEqual(response.status_code, 200)
        data = response.data
        model = dict(data[0])

        self.api_client.force_authenticate(user=user)
        response = self.api_client.get(
            reverse('multifactor-drf-totp-detail', kwargs={"pk": model['id']}),
            format='json'
        )
        self.assertEqual(response.status_code, 200)

    def test_numbers(self) -> None:
        user = User.objects.create_user(
            username='testuser2',
            email='testuser2@example.com',
            password='123456'
        )
        self.api_client.force_authenticate(user=user)

        secret_key = "K3JJGXLOIPLYED3X"
        totp = pyotp.TOTP(secret_key)
        answer = totp.now()

        response = self.api_client.post(
            reverse('multifactor-drf-totp-list'),
            format='json',
            data={
                "answer": answer,
                "secret_key": secret_key,
            }
        )
        self.assertEqual(response.status_code, 201)
